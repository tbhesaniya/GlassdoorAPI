﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GlassdoorAPI.Models;
using System.Web.Http.Cors;

namespace GlassdoorAPI.Controllers.ReviewsCompany
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JobsCompanyReviewsController : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/JobsCompanyReviews
        public IQueryable<JobsReview> GetJobsReviews() 
        {
            return db.JobsReviews;
        }

        // GET: api/JobsCompanyReviews/5
        [ResponseType(typeof(JobsReview))]
        public IHttpActionResult GetJobsReview(int id)
        { 
            JobsReview jobsReview = db.JobsReviews.Find(id);
            /*if (jobsReview == null)
            {
                return NotFound();
            }*/

            var reviews = from rev in db.JobsReviews
                          
                          
                          join comp in db.Companies on rev.CompanyID equals comp.CompanyID
                          where comp.CompanyID == id
                          orderby rev.ReviewID descending
                          select new JobsReviewCL
                       {
                              ReviewID = rev.ReviewID,
                           Comments = rev.Comments,
                           Name = rev.Name,
                           Profession = rev.Profession,
                           DateT = rev.DateT,
                           CompanyName = comp.CompanyName
                          };

            return Ok(reviews);

        }

        // PUT: api/JobsCompanyReviews/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutJobsReview(int id, JobsReview jobsReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != jobsReview.ReviewID)
            {
                return BadRequest();
            }

            db.Entry(jobsReview).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobsReviewExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/JobsCompanyReviews
        [ResponseType(typeof(JobsReview))]
        public IHttpActionResult PostJobsReview(JobsReview jobsReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.JobsReviews.Add(jobsReview);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = jobsReview.ReviewID }, jobsReview);
        }

        // DELETE: api/JobsCompanyReviews/5
        [ResponseType(typeof(JobsReview))]
        public IHttpActionResult DeleteJobsReview(int id)
        {
            JobsReview jobsReview = db.JobsReviews.Find(id);
            if (jobsReview == null)
            {
                return NotFound();
            }

            db.JobsReviews.Remove(jobsReview);
            db.SaveChanges();

            return Ok(jobsReview);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobsReviewExists(int id)
        {
            return db.JobsReviews.Count(e => e.ReviewID == id) > 0;
        }
    }
}