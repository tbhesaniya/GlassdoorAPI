using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using GlassdoorAPI.Models;

namespace GlassdoorAPI.Controllers.D_Jobs
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class djobController : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/djob
        public IQueryable<Job> GetJobs()
        {
            return db.Jobs;
        }

        // GET: api/djob/5
        [ResponseType(typeof(Job))]
        public IHttpActionResult GetJob(int id)
        {
            //adasdddddddddddddddddddddddd
            Job job = db.Jobs.Find(id);
            var jobsID = from jobX in db.Jobs
                         where jobX.JobID == id
                         join comp in db.Companies on jobX.CompanyID equals comp.CompanyID
                       join state in db.States on comp.StateID equals state.StateID
                       join country in db.Countries on comp.CountryID equals country.CountryID
                       select new JobCL
                       {

                           JobID = jobX.JobID,
                           JobName = jobX.JobName,
                           JobDescription = jobX.JobDescription,
                           CompanyID = jobX.CompanyID,
                           Company = comp.CompanyName,
                           DateCreated2 = jobX.DateCreated,
                           Status = jobX.Status,
                           Address = jobX.Address,
                           City = jobX.City,
                           State = state.State1,
                           Country = country.Country1
                       };

            return Ok(job);
        }
        [Route("api/djob/UpdateCustomer")]
        [HttpPost]
        public bool UpdateCustomer(Job task)
        {
            using (RecruitmentManagementEntities entities = new RecruitmentManagementEntities())
            {
                Job updatedtask = (from c in entities.Jobs
                                    where c.JobID == task.JobID
                                    select c).FirstOrDefault();
                updatedtask.DateCreated = task.DateCreated;
                updatedtask.JobName = task.JobName;
                updatedtask.Address = task.Address;
                entities.SaveChanges();
            }

            return true;
        }
        // PUT: api/djob/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutJob(int id, Job job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != job.JobID)
            {
                return BadRequest();
            }

            db.Entry(job).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/djob
        [ResponseType(typeof(Job))]
        public IHttpActionResult PostJob(Job job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Jobs.Add(job);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = job.JobID }, job);
        }

        // DELETE: api/djob/5
        [ResponseType(typeof(Job))]
        public IHttpActionResult DeleteJob(int id)
        {
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return NotFound();
            }

            db.Jobs.Remove(job);
            db.SaveChanges();

            return Ok(job);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobExists(int id)
        {
            return db.Jobs.Count(e => e.JobID == id) > 0;
        }
    }
}