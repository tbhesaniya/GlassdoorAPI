﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GlassdoorAPI.Models;
using System.Web.Http.Cors;

namespace GlassdoorAPI.Controllers.ReplyCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReplyReviewsController : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/ReplyReviews
        public IQueryable<ReplyReview> GetReplyReviews()
        {
            return db.ReplyReviews;
        }

        // GET: api/ReplyReviews/5
        [ResponseType(typeof(ReplyReview))]
        public IHttpActionResult GetReplyReview(int id)
        {
            ReplyReview replyReview = db.ReplyReviews.Find(id);
            if (replyReview == null)
            {
                return NotFound();
            }

            return Ok(replyReview);
        }

        // PUT: api/ReplyReviews/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutReplyReview(int id, ReplyReview replyReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != replyReview.ReplyReviewID)
            {
                return BadRequest();
            }

            db.Entry(replyReview).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReplyReviewExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ReplyReviews
        [ResponseType(typeof(ReplyReview))]
        public IHttpActionResult PostReplyReview(ReplyReview replyReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ReplyReviews.Add(replyReview);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = replyReview.ReplyReviewID }, replyReview);
        }

        // DELETE: api/ReplyReviews/5
        [ResponseType(typeof(ReplyReview))]
        public IHttpActionResult DeleteReplyReview(int id)
        {
            ReplyReview replyReview = db.ReplyReviews.Find(id);
            if (replyReview == null)
            {
                return NotFound();
            }

            db.ReplyReviews.Remove(replyReview);
            db.SaveChanges();

            return Ok(replyReview);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReplyReviewExists(int id)
        {
            return db.ReplyReviews.Count(e => e.ReplyReviewID == id) > 0;
        }
    }
}