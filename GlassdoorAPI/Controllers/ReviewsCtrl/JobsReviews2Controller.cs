﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GlassdoorAPI.Models;
using System.Web.Http.Cors;

namespace GlassdoorAPI.Controllers.ReviewsCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JobsReviews2Controller : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/JobsReviews
        public IQueryable<ReviewsCL> GetJobsReviews()
        {

            var reviews = from rev in db.JobsReviews
                          join comp in db.Companies on rev.CompanyID equals comp.CompanyID
                          join replayReview in db.ReplyReviews on rev.ReviewID equals replayReview.ReviewID
                            into joinDeptEmp
                          from Jobrevi in joinDeptEmp.DefaultIfEmpty()
                          select new ReviewsCL
                          {
                              ReviewID = rev.ReviewID,
                              Raiting = rev.Raiting,
                              DateT = rev.DateT,
                              Comments = rev.Comments,
                              Name = rev.Name,
                              Profession = rev.Profession,
                              CompanyName = comp.CompanyName,
                              ReplayReviewsComments = (from RR in db.ReplyReviews where RR.ReviewID == rev.ReviewID select RR.Comments).ToList()

                          };

            return reviews;

        }

        // GET: api/JobsReviews/5
        [ResponseType(typeof(JobsReview))]
        public IHttpActionResult GetJobsReview(int id)
        {
            JobsReview jobsReview = db.JobsReviews.Find(id);

            var reviewsid = from rev in db.JobsReviews
                            where rev.ReviewID == id
                            join comp in db.Companies on rev.CompanyID equals comp.CompanyID
                            join replayReview in db.ReplyReviews on rev.ReviewID equals replayReview.ReviewID
                            into joinDeptEmp
                            select new ReviewsCL
                            {
                                ReviewID = rev.ReviewID,
                                Raiting = rev.Raiting,
                                DateT = rev.DateT,
                                Comments = rev.Comments,
                                Name = rev.Name,
                                Profession = rev.Profession,
                                CompanyName = comp.CompanyName,
                                ReplayReviewsComments = (from RR in db.ReplyReviews where RR.ReviewID == rev.ReviewID select RR.Comments).ToList()
                            };


            /*if (jobsReview == null)
            {
                return NotFound();
            }*/

            return Ok(reviewsid);
        }

        // PUT: api/JobsReviews/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutJobsReview(int id, JobsReview jobsReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != jobsReview.ReviewID)
            {
                return BadRequest();
            }

            db.Entry(jobsReview).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobsReviewExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/JobsReviews
        [ResponseType(typeof(JobsReview))]
        public IHttpActionResult PostJobsReview(JobsReview jobsReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.JobsReviews.Add(jobsReview);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = jobsReview.ReviewID }, jobsReview);
        }

        // DELETE: api/JobsReviews/5
        [ResponseType(typeof(JobsReview))]
        public IHttpActionResult DeleteJobsReview(int id)
        {
            JobsReview jobsReview = db.JobsReviews.Find(id);
            if (jobsReview == null)
            {
                return NotFound();
            }

            db.JobsReviews.Remove(jobsReview);
            db.SaveChanges();

            return Ok(jobsReview);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobsReviewExists(int id)
        {
            return db.JobsReviews.Count(e => e.ReviewID == id) > 0;
        }
    }
}