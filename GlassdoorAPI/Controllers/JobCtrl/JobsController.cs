﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GlassdoorAPI.Models;
using System.Web.Http.Cors;

namespace GlassdoorAPI.Controllers.JobCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JobsController : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/Jobs
        public IQueryable<JobCL> GetJobs()
        {
            var jobs = from job in db.Jobs
                       join comp in db.Companies on job.CompanyID equals comp.CompanyID
                       join state in db.States on job.StateID equals state.StateID
                       join country in db.Countries on job.CountryID equals country.CountryID
                       orderby job.JobID descending
                       select new JobCL
                       {

                           JobID = job.JobID,
                           JobName = job.JobName,
                           JobDescription = job.JobDescription,
                           //JobDescription = job.JobDescription.Substring(0, 100) + "...",
                           CompanyID = job.CompanyID,
                           Company = comp.CompanyName,
                           DateCreated2 = job.DateCreated,
                           Status = job.Status,
                           Address = job.Address,
                           City = job.City,
                           State = state.State1,
                           StateID = job.StateID,
                           Country = country.Country1,
                           CountryID = job.CountryID,
                           DaysSponsered = job.DaysSponsered
                       };

            return jobs;
        }
        [Route("Api/Jobs/GetJobs2")]
        public List<JobCL> GetJobs2()
        { 
            var jobs = from job in db.Jobs
                       join comp in db.Companies on job.CompanyID equals comp.CompanyID
                       join state in db.States on comp.StateID equals state.StateID
                       join country in db.Countries on comp.CountryID equals country.CountryID
                       orderby job.JobID descending
                       select new JobCL
                       {

                           JobID = job.JobID,
                           JobName = job.JobName,
                           JobDescription = job.JobDescription,
                           //JobDescription = job.JobDescription.Substring(0, 100) + "...",
                           CompanyID = job.CompanyID,
                           Company = comp.CompanyName,
                           DateCreated2 = job.DateCreated,
                           Status = job.Status,
                           Address = job.Address,
                           City = job.City,
                           State = state.State1,
                           StateID = job.StateID,
                           Country = country.Country1,
                           CountryID = job.CountryID,
                           DaysSponsered = job.DaysSponsered
                       };
            List<JobCL> res = new List<JobCL>();
            res = jobs.ToList();
            return res;
        }
        // GET: api/Jobs/5
        [ResponseType(typeof(Job))]
        public IHttpActionResult GetJob(int id)
        {
            Job job = db.Jobs.Find(id);

            var jobsID = from jobX in db.Jobs
                         where jobX.JobID == id
                         join comp in db.Companies on jobX.CompanyID equals comp.CompanyID
                         join state in db.States on job.StateID equals state.StateID
                         join country in db.Countries on job.CountryID equals country.CountryID
                         select new JobCL
                         {

                             JobID = jobX.JobID,
                             JobName = jobX.JobName,
                             JobDescription = jobX.JobDescription,
                             CompanyID = jobX.CompanyID,
                             Company = comp.CompanyName,
                             DateCreated2 = jobX.DateCreated,
                             Status = jobX.Status,
                             Address = jobX.Address,
                             City = jobX.City,
                             CountryID = jobX.CountryID,
                             StateID = jobX.StateID,
                             State = state.State1,
                             Country = country.Country1,
                             DaysSponsered = job.DaysSponsered,
                             Clicks = job.Clicks
                         };

            if (job == null)
            {
                return NotFound();
            }

            return Ok(jobsID);
        }

        // PUT: api/Jobs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutJob(int id, Job job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != job.JobID)
            {
                return BadRequest();
            }

            db.Entry(job).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Jobs
        [ResponseType(typeof(Job))]
        public IHttpActionResult PostJob(Job job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Jobs.Add(job);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = job.JobID }, job);
        }

        // DELETE: api/Jobs/5
        [ResponseType(typeof(Job))]
        public IHttpActionResult DeleteJob(int id)
        {
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return NotFound();
            }

            db.Jobs.Remove(job);
            db.SaveChanges();

            return Ok(job);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobExists(int id)
        {
            return db.Jobs.Count(e => e.JobID == id) > 0;
        }

        //[Route("Api/Jobs/Getlistcity")]
        //public IHttpActionResult Getlistcity()
        //{
        //    var li = db.Jobs.AsEnumerable().ToList();
        //    List<string> res = new List<string>();
        //    foreach (var a in li)
        //    {
        //        res.Add(a.City);
        //    }
        //    return Ok(res);
        //}

        [Route("Api/Jobs/Getlistcity")]
        public IHttpActionResult Getlistcity()
        {
            var li = db.Jobs.AsEnumerable().ToList();
            List<string> res = new List<string>();
            foreach (var a in li)
            {
                if (!res.Contains(a.City))
                {
                    res.Add(a.City);
                }
            }
            return Ok(res);
        }


        [Route("Api/Jobs/Getliststate")]
        public IHttpActionResult Getlistate()
        {
            var li = db.States.AsEnumerable().ToList();
            List<string> state = new List<string>();
            foreach (var s in li)
            {
                state.Add(s.State1);
            }
            return Ok(state);
        }



    }
}