﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GlassdoorAPI.Models;
using System.Web.Http.Cors;

namespace GlassdoorAPI.Controllers.CompanySectorCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CompanySectorsController : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/CompanySectors
        public IQueryable<CompanySector> GetCompanySectors()
        {
            return db.CompanySectors;
        }

        // GET: api/CompanySectors/5
        [ResponseType(typeof(CompanySector))]
        public IQueryable<CompanySector> GetCompanySector(int id)
        {
            return db.CompanySectors.Where(o => o.CompanyTypeID == id);
        }

        // PUT: api/CompanySectors/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCompanySector(int id, CompanySector companySector)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != companySector.CompanySectorID)
            {
                return BadRequest();
            }

            db.Entry(companySector).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanySectorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CompanySectors
        [ResponseType(typeof(CompanySector))]
        public IHttpActionResult PostCompanySector(CompanySector companySector)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CompanySectors.Add(companySector);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = companySector.CompanySectorID }, companySector);
        }

        // DELETE: api/CompanySectors/5
        [ResponseType(typeof(CompanySector))]
        public IHttpActionResult DeleteCompanySector(int id)
        {
            CompanySector companySector = db.CompanySectors.Find(id);
            if (companySector == null)
            {
                return NotFound();
            }

            db.CompanySectors.Remove(companySector);
            db.SaveChanges();

            return Ok(companySector);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanySectorExists(int id)
        {
            return db.CompanySectors.Count(e => e.CompanySectorID == id) > 0;
        }
    }
}