﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GlassdoorAPI.Models;
using System.Web.Http.Cors;

namespace GlassdoorAPI.Controllers.JobsCompanyCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JobsCompanyController : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/JobsCompany
        public IQueryable<Job> GetJobs()
        {
            return db.Jobs;
        }

        // GET: api/JobsCompany/5
        [ResponseType(typeof(Job))]
        public IHttpActionResult GetJob(int id)
        {
            /*Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return NotFound();
            }*/

            var jobs = from job in db.Jobs
                            where job.CompanyID == id
                           join state in db.States on job.StateID equals state.StateID
                           join country in db.Countries on job.CountryID equals country.CountryID
                           join comp in db.Companies on job.CompanyID equals comp.CompanyID
                           orderby job.JobID descending 
                       select new JobCL
                            {
                                JobID = job.JobID,
                                JobName = job.JobName,
                                JobDescription = job.JobDescription,
                                CompanyID = job.JobID,
                                DateCreated2 = job.DateCreated,
                                Status = job.Status,
                                Address = job.Address,
                                City = job.City,
                                StateID = job.StateID,
                                CountryID = job.CountryID,
                                DaysSponsered = job.DaysSponsered,
                                State = state.State1,
                                Country = country.Country1,
                                Company = comp.CompanyName,
                                Clicks = job.Clicks
                       };

            return Ok(jobs);
        }

        // PUT: api/JobsCompany/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutJob(int id, Job job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != job.JobID)
            {
                return BadRequest();
            }

            db.Entry(job).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/JobsCompany
        [ResponseType(typeof(Job))]
        public IHttpActionResult PostJob(Job job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Jobs.Add(job);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = job.JobID }, job);
        }

        // DELETE: api/JobsCompany/5
        [ResponseType(typeof(Job))]
        public IHttpActionResult DeleteJob(int id)
        {
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return NotFound();
            }

            db.Jobs.Remove(job);
            db.SaveChanges();

            return Ok(job);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobExists(int id)
        {
            return db.Jobs.Count(e => e.JobID == id) > 0;
        }
    }
}