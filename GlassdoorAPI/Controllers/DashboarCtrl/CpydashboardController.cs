﻿using GlassdoorAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GlassdoorAPI.Controllers.DashboardCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CpydashboardController : ApiController
    {
        
        private string connectionString = ConfigurationManager.ConnectionStrings["GlassdoreConn"].ToString();

        [HttpGet]
        public List<JobCL> GetJobs(int companyID)
        {
            List<JobCL> job_list = new List<JobCL>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = @"select j.CompanyID, j.JobID, j.JobName, j.JobDescription, j.DateCreated, j.Status, 
                                j.Address, j.City, s.State ,c.Country, j.DaysSponsered, j.Clicks
                                from Jobs as j, State as s, Country as c
                                where j.StateID = s.StateID and j.CountryID = c.CountryID and j.CompanyID = @companyID";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@companyID", companyID);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                connection.Open();
                da.Fill(dt);
                connection.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    job_list.Add(
                        new JobCL
                        {
                            CompanyID = Convert.ToInt16(dr["CompanyID"]),
                            JobID = Convert.ToInt16(dr["JobID"]),
                            Name = Convert.ToString(dr["JobName"]),
                            Description = Convert.ToString(dr["JobDescription"]),
                            DateCreated = Convert.ToString(dr["DateCreated"]),
                            Status = Convert.ToString(dr["Status"]),
                            Address = Convert.ToString(dr["Address"]),
                            City = Convert.ToString(dr["City"]),
                            State = Convert.ToString(dr["State"]),
                            Country = Convert.ToString(dr["Country"]),
                            DaysSponsered = Convert.ToInt16(dr["DaysSponsered"]),
                            Clicks = Convert.ToInt16(dr["Clicks"])
                        }
                    );
                }
            }
            return job_list;
    
        }

        [HttpGet]
        public List<companyInfo> GetCompanyInfo(int companyID)
        {
            List<companyInfo> info_list = new List<companyInfo>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = @"select c.CompanyID, c.CompanyName, c.Address, 
                                c.City, s.State, cou.Country, c.Phone, c.Website, 
                                cs.CompanySector, ct.CompanyType,
                                c.CompanyStatus, c.CompanyContactName, c.Username, c.Email
                                from Company as c, State as s, Country as cou, CompanySector as cs, CompanyType as ct
                                where c.StateID = s.StateID and
                                c.CountryID = cou.CountryID and
                                cs.CompanySectorID = c.CompanySectorID and
                                ct.CompanyTypeID = c.CompanyTypeID and CompanyID=@companyID";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@companyID", companyID);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                connection.Open();
                da.Fill(dt);
                connection.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    info_list.Add(
                        new companyInfo
                        {
                            CompanyID = Convert.ToInt16(dr["CompanyID"]),
                            Name = Convert.ToString(dr["CompanyName"]),
                            Address = Convert.ToString(dr["Address"]),
                            City = Convert.ToString(dr["City"]),
                            State = Convert.ToString(dr["State"]),
                            Country = Convert.ToString(dr["Country"]),
                            Phone = Convert.ToString(dr["Phone"]),
                            Website = Convert.ToString(dr["Website"]),
                            Sector = Convert.ToString(dr["CompanySector"]),
                            Type = Convert.ToString(dr["CompanyType"]),
                            Status = Convert.ToString(dr["CompanyStatus"]),
                            ContactName = Convert.ToString(dr["CompanyContactName"]),
                            Username = Convert.ToString(dr["Username"]),
                            Email = Convert.ToString(dr["Email"])
                        });
                }

            }
            return info_list;
        }

        [HttpGet]
        public List<ReviewsCL> GetReviewsAndReplies(int companyID)
        {
            List<ReviewsCL> review_list = new List<ReviewsCL>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = @"select j.CompanyID, jr.JobID , jr.ReviewID, j.JobName, CONCAT(e.Name ,' ' ,e.LastName) as Author, jr.Raiting, jr.DateT as 'CDate', jr.Comments, rr.Comments as 'Reply', rr.DateT as 'RDate'
                                from JobsReviews as jr, Jobs as j, Employee as e, ReplyReviews as rr
                                where jr.JobID = j.JobID and e.EmployeeID = jr.EmployeeID and jr.ReviewID = rr.ReviewID and
                                CompanyID=@companyID";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@companyID", companyID);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                connection.Open();
                da.Fill(dt);
                connection.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    review_list.Add(
                        new ReviewsCL
                        {
                            CompanyID = Convert.ToInt16(dr["CompanyID"]),
                            JobID = Convert.ToInt16(dr["JobID"]),
                            ReviewID = Convert.ToInt16(dr["ReviewID"]),
                            JobName = Convert.ToString(dr["JobName"]),
                            Author = Convert.ToString(dr["Author"]),
                            Rating = Convert.ToInt16(dr["Raiting"]),
                            Comments = Convert.ToString(dr["Comments"]),
                            Reply = Convert.ToString(dr["Reply"])
                        }

                        );
                }
            }
            return review_list;
        }

       
    }
}
