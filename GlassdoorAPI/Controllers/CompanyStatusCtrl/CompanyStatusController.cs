﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GlassdoorAPI.Models;
using System.Web.Http.Cors;

namespace GlassdoorAPI.Controllers.CompanyStatusCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CompanyStatusController : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/CompanyStatus
        public IQueryable<CompanyStatu> GetCompanyStatus()
        {
            return db.CompanyStatus;
        }

        // GET: api/CompanyStatus/5
        [ResponseType(typeof(CompanyStatu))]
        public IHttpActionResult GetCompanyStatu(int id)
        {
            CompanyStatu companyStatu = db.CompanyStatus.Find(id);
            if (companyStatu == null)
            {
                return NotFound();
            }

            return Ok(companyStatu);
        }

        // PUT: api/CompanyStatus/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCompanyStatu(int id, CompanyStatu companyStatu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != companyStatu.CompanyStatusID)
            {
                return BadRequest();
            }

            db.Entry(companyStatu).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyStatuExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CompanyStatus
        [ResponseType(typeof(CompanyStatu))]
        public IHttpActionResult PostCompanyStatu(CompanyStatu companyStatu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CompanyStatus.Add(companyStatu);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = companyStatu.CompanyStatusID }, companyStatu);
        }

        // DELETE: api/CompanyStatus/5
        [ResponseType(typeof(CompanyStatu))]
        public IHttpActionResult DeleteCompanyStatu(int id)
        {
            CompanyStatu companyStatu = db.CompanyStatus.Find(id);
            if (companyStatu == null)
            {
                return NotFound();
            }

            db.CompanyStatus.Remove(companyStatu);
            db.SaveChanges();

            return Ok(companyStatu);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanyStatuExists(int id)
        {
            return db.CompanyStatus.Count(e => e.CompanyStatusID == id) > 0;
        }
    }
}