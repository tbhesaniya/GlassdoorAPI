﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GlassdoorAPI.Models;
using System.Web.Http.Cors;
using System.Collections;
using System.Threading.Tasks;
using System.Web;
using System.Text;

namespace GlassdoorAPI.Controllers.CompanyCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CompaniesController : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/Companies
        public IQueryable<CompanyCL> GetCompanies() 
        {
            var company = from comp in db.Companies


                          join state in db.States on comp.StateID equals state.StateID into st from ST in st.DefaultIfEmpty()
                      join country in db.Countries on comp.CountryID equals country.CountryID into ct from CT in ct.DefaultIfEmpty()
                          orderby comp.CompanyID descending
                          select new CompanyCL { 

                          CompanyID = comp.CompanyID,
                          CompanyName = comp.CompanyName,
                          Address = comp.Address,
                          City = comp.City,
                          StateID = comp.StateID,
                          State = ST.State1,
                          CountryID = comp.CountryID,
                          Country = CT.Country1,
                          Phone = comp.Phone,
                          Website = comp.Website,
                          CompanyTypeID = comp.CompanyTypeID,
                          CompanySectorID = comp.CompanySectorID,
                          CompanyStatus = comp.CompanyStatus,
                          CompanyContactName = comp.CompanyContactName,
                          Username = comp.Username,
                          Password = comp.Password,
                          IdConfirmUser = comp.IdConfirmUser,
                          SecurityCode = comp.SecurityCode,
                          CompanyStatusID = comp.CompanyStatusID,
                          Email = comp.Email,
                          Description = comp.Description


                          }; 

            return company;
        }
         
        // GET: api/Companies/5
        [ResponseType(typeof(CompanyCL))] 
        public IHttpActionResult GetCompany(int id)
        {
            Company company = db.Companies.Find(id);
            var companyId = from comp in db.Companies
                            select new CompanyCL
                            {};
            
                companyId = from comp in db.Companies
                                where comp.CompanyID == id
                                join state in db.States on comp.StateID equals state.StateID into st from ST in st.DefaultIfEmpty()
                                join country in db.Countries on comp.CountryID equals country.CountryID into ctX from CTX in ctX.DefaultIfEmpty()
                                join ct in db.CompanyTypes on comp.CompanyTypeID equals ct.CompanyTypeID into ct from CT in ct.DefaultIfEmpty()
                                join cs in db.CompanySectors on comp.CompanySectorID equals cs.CompanySectorID into cs from CS in cs.DefaultIfEmpty()
                            select new CompanyCL
                                {

                                    CompanyID = comp.CompanyID,
                                    CompanyName = comp.CompanyName,
                                    Address = comp.Address,
                                    City = comp.City,
                                    StateID = comp.StateID,
                                    State = ST.State1,
                                    CountryID = comp.CountryID,
                                    Country = CTX.Country1,
                                    Phone = comp.Phone,
                                    Website = comp.Website,
                                    CompanyType = CT.CompanyType1,
                                    CompanyTypeID = comp.CompanyTypeID,
                                    CompanySector = CS.CompanySector1,
                                    CompanySectorID = comp.CompanySectorID,
                                    CompanyStatus = comp.CompanyStatus,
                                    CompanyContactName = comp.CompanyContactName,
                                    Username = comp.Username,
                                    Password = comp.Password,
                                    IdConfirmUser = comp.IdConfirmUser,
                                    SecurityCode = comp.SecurityCode,
                                    CompanyStatusID = comp.CompanyStatusID,
                                    Email = comp.Email,
                                    Description = comp.Description


                                };
            
            


            if (company == null)
            {
                return NotFound();
            }

            return Ok(companyId);
        }

        // PUT: api/Companies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCompany(int id, Company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != company.CompanyID)
            {
                return BadRequest();
            }

            db.Entry(company).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Companies
        [ResponseType(typeof(Company))]
        public IHttpActionResult PostCompany(Company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Companies.Add(company);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = company.CompanyID }, company);
        }
        

        // DELETE: api/Companies/5
        [ResponseType(typeof(Company))]
        public IHttpActionResult DeleteCompany(int id)
        {
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return NotFound();
            }

            db.Companies.Remove(company);
            db.SaveChanges();

            return Ok(company);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanyExists(int id)
        {
            return db.Companies.Count(e => e.CompanyID == id) > 0;
        }
    }
}