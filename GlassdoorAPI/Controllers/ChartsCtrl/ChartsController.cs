﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using GlassdoorAPI.Models;

namespace GlassdoorAPI.Controllers.ChartsCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ChartsController : ApiController
    {

        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();


        // GET: api/Employees
        //public IQueryable<Employee> GetEmployees()
        //{
        //    return db.Employees;
        //}
        [Route("second/Charts/Getmale")]

        public Array Getmale()
        {

            CompanyCL cl = new CompanyCL();
            List<CompanyCL> li = new List<CompanyCL>();
            var que = from i in db.Companies where i.City == "Miami" select i;
            int[] res = new int[2];
            res[0] = que.Count();
            res[1] = 2;

            return res;            
        }
        [Route("second/Charts/Getjobsbymonth")]

        public Array Getjobsbymonth()
        {
            JobCL jb = new JobCL();
            List<JobCL> li = new List<JobCL>();
            Dictionary<int, int> dic = new Dictionary<int, int>();
            for(int i = 0; i < 12; i++)
            {
                dic[i] = 0;
            }
            int[] res = new int[12];
            var jobcreate = db.Jobs.AsEnumerable().ToArray();
            foreach (var a in jobcreate)
            {
                dic[a.DateCreated.Value.Month-1]++;
            }
            int num = jobcreate.Count();

            for (int i = 0; i < 12; i++)
            {
                res[i] = dic[i];
            }
  
            return res;

        }
        [Route("second/Charts/Getcompanylist")]

        public List<string> Getcompanylist()
        {
            var compname = db.Companies.AsEnumerable().ToArray();
            List<string> uniqueValues = new List<string>();

            for (int i = 0; i < compname.Count(); i++)
            {
                if (!uniqueValues.Contains(compname[i].CompanyName))
                    uniqueValues.Add(compname[i].CompanyName);
            }
            int n = uniqueValues.Count();
            return uniqueValues;
        }

        //[Route("Api/Charts/Getlabel1/{select}")]
        public IHttpActionResult Getlabel1(string select)
        {
            List<string> label = new List<string>();
            List<int> data = new List<int>();
            if (select == "JobLocation")
            {
                JobCL jb = new JobCL();
                List<JobCL> li = new List<JobCL>();
                //var jobloca = db.Jobs.AsEnumerable().ToArray();

                var jobloca = (from j in db.Jobs
                               join s in db.States on j.StateID equals s.StateID
                               select s).AsEnumerable().ToArray();

                Dictionary<string, int> dic = new Dictionary<string, int>();


                foreach (var i in jobloca)
                {
                    dic[i.State1] = 0;

                }
                foreach (var i in jobloca)
                {
                    dic[i.State1]++;

                }

                label = dic.Keys.ToList();
                data = dic.Values.ToList();
            }


            else if (select == "JobReview")
            {
                var tmp = (from j in db.Companies
                           join r in db.JobsReviews on j.CompanyID equals r.CompanyID
                           select new { j.CompanyName, r.Raiting });
                Dictionary<string, int> dic = new Dictionary<string, int>();

                foreach (var i in tmp)
                {
                    if (!string.IsNullOrEmpty(i.CompanyName) && i.Raiting >= 0) dic[i.CompanyName] = 0;

                }
                foreach (var i in tmp)
                {
                    if (!string.IsNullOrEmpty(i.CompanyName) && i.Raiting >= 0) dic[i.CompanyName]++;
                }
                label = dic.Keys.ToList();
            }
            else
            {
                var jobloca = (from j in db.Jobs
                               join c in db.Companies on j.CompanyID equals c.CompanyID
                               select new { j.JobName, c.CompanyName }).AsEnumerable().ToArray();

                Dictionary<string, int?> dic = new Dictionary<string, int?>();


                foreach (var i in jobloca)
                {
                    dic[i.CompanyName] = 0;

                }
                foreach (var i in jobloca)
                {
                    dic[i.CompanyName]++;

                }

                label = dic.Keys.ToList();
                
            };
  
            return Ok(label);
        }

        //[Route("Api/Charts/Getdata1/{select}")]
        public IHttpActionResult Getdata1(string select)
        {
            List<string> label = new List<string>();
            List<int?> data = new List<int?>();
            if (select == "JobLocation")
            {
                JobCL jb = new JobCL();
                List<JobCL> li = new List<JobCL>();
                //var jobloca = db.Jobs.AsEnumerable().ToArray();

                var jobloca = (from j in db.Jobs
                               join s in db.States on j.StateID equals s.StateID
                               select s).AsEnumerable().ToArray();

                Dictionary<string, int?> dic = new Dictionary<string, int?>();


                foreach (var i in jobloca)
                {
                    dic[i.State1] = 0;

                }
                foreach (var i in jobloca)
                {
                    dic[i.State1]++;

                }

                label = dic.Keys.ToList();
                data = dic.Values.ToList();
            }


            else if (select == "JobReview")
            {
                var tmp = (from j in db.Companies
                           join r in db.JobsReviews on j.CompanyID equals r.CompanyID
                           select new { j.CompanyName, r.Raiting });
                Dictionary<string, int?> dic = new Dictionary<string, int?>();
                Dictionary<string, int?> dic2 = new Dictionary<string, int?>();
                foreach (var i in tmp)
                {
                    if (!string.IsNullOrEmpty(i.CompanyName) && i.Raiting >= 0) dic[i.CompanyName] = 0;
                    if (!string.IsNullOrEmpty(i.CompanyName) && i.Raiting >= 0) dic2[i.CompanyName] = 0;

                }
                foreach (var i in tmp)
                {
                    if (!string.IsNullOrEmpty(i.CompanyName) && i.Raiting >= 0) dic[i.CompanyName]++;
                    if (!string.IsNullOrEmpty(i.CompanyName) && i.Raiting >= 0) dic2[i.CompanyName] += i.Raiting;

                }
                List<int?> data1 = new List<int?>();
                List<int?> data2 = new List<int?>();
                data1 = dic.Values.ToList();
                data2 = dic2.Values.ToList();
                int num = data1.Count();
                for (int i = 0; i < num; i++)
                {
                    data.Add(data2[i] / data1[i]);
                }
            }
            else
            {
                var jobloca = (from j in db.Jobs
                               join c in db.Companies on j.CompanyID equals c.CompanyID
                               select new { j.JobName, c.CompanyName }).AsEnumerable().ToArray();

                Dictionary<string, int?> dic = new Dictionary<string, int?>();


                foreach (var i in jobloca)
                {
                    dic[i.CompanyName] = 0;

                }
                foreach (var i in jobloca)
                {
                    dic[i.CompanyName]++;

                }

                label = dic.Keys.ToList();
                data = dic.Values.ToList();
            };

            return Ok(data);
        }
        public IHttpActionResult Getcompare1(string select1)
        {
            int compare1 = 0;
        
                var cmp1 = (from j in db.Jobs
                            join c in db.Companies on j.CompanyID equals c.CompanyID
                            join s in db.States on j.StateID equals s.StateID
                            where (c.CompanyName == select1 || s.State1==select1)
                            select c).AsEnumerable().ToArray();
            
            compare1 = cmp1.Count();
            
           
            return Ok(compare1);
            
        }
       [Route("second/Charts/Getcompanyname")]
        public List<string> Getcompanyname()
        {
            Dictionary<string, int> dic = new Dictionary<string, int>();
            List<string> res = new List<string>();
            var tmp = (from j in db.Jobs
                       join c in db.Companies on j.CompanyID equals c.CompanyID
                       select new { c.CompanyName, j.Clicks }).AsEnumerable().ToList();
           foreach(var i in tmp)
            {
                dic[i.CompanyName] = 0;
            }
            foreach (var i in tmp)
            {
                dic[i.CompanyName] ++;
            }
            res = dic.Keys.ToList();
            return res;
        }
        [Route("second/Charts/Getcompanyclicks")]

        public List<int?> Getcompanyclicks()
        {
            Dictionary<string, int?> dic = new Dictionary<string, int?>();

            List<int?> res = new List<int?>();
            var tmp = (from j in db.Jobs
                       join c in db.Companies on j.CompanyID equals c.CompanyID
                       select new { c.CompanyName, j.Clicks }).AsEnumerable().ToList();
            foreach (var i in tmp)
            {
                dic[i.CompanyName] = 0;
            }
            foreach (var i in tmp)
            {
                dic[i.CompanyName]+=i.Clicks;
            }
            res = dic.Values.ToList();
            
            return res;
        }
    }
}