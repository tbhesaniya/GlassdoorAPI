﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GlassdoorAPI.Models;
using System.Web.Http.Cors;

namespace GlassdoorAPI.Controllers.LoginCtrl
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {
        private RecruitmentManagementEntities db = new RecruitmentManagementEntities();

        // GET: api/Login
        public IQueryable<Company> GetCompanies()
        {
            return db.Companies;
        }

        // GET: api/Login/5
        [ResponseType(typeof(Company))]
        public IHttpActionResult GetCompany(int id)
        {
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return NotFound();
            }

            return Ok(company);
        }

        // PUT: api/Login/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCompany(int id, Company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != company.CompanyID)
            {
                return BadRequest();
            }

            db.Entry(company).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Login
        [ResponseType(typeof(Company))]
        public IHttpActionResult PostCompany(Company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var companyId = from comp in db.Companies 
                            where comp.Email == company.Email && comp.Password == company.Password
                            /*join state in db.States on comp.StateID equals state.StateID into xx from XX  
                            join country in db.Countries on comp.CountryID equals country.CountryID
                            join ct in db.CompanyTypes on comp.CompanyTypeID equals ct.CompanyTypeID
                            join cs in db.CompanySectors on comp.CompanySectorID equals cs.CompanySectorID*/
                            select new CompanyCL
                            {

                                CompanyID = comp.CompanyID,
                                CompanyName = comp.CompanyName
                                /*Address = comp.Address,
                                City = comp.City,
                                StateID = comp.StateID,
                                State = state.State1,
                                CountryID = comp.CountryID,
                                Country = country.Country1,
                                Phone = comp.Phone,
                                Website = comp.Website,
                                CompanyType = ct.CompanyType1,
                                CompanySector = cs.CompanySector1,
                                CompanyStatus = comp.CompanyStatus,
                                CompanyContactName = comp.CompanyContactName,
                                Username = comp.Username,
                                Password = comp.Password,
                                IdConfirmUser = comp.IdConfirmUser,
                                SecurityCode = comp.SecurityCode,
                                CompanyStatusID = comp.CompanyStatusID,
                                Email = comp.Email*/


                            };

            //db.Companies.Add(company);
            //db.SaveChanges();

            return Ok(companyId);
        }

        // DELETE: api/Login/5
        [ResponseType(typeof(Company))]
        public IHttpActionResult DeleteCompany(int id)
        {
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return NotFound();
            }

            db.Companies.Remove(company);
            db.SaveChanges();

            return Ok(company);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanyExists(int id)
        {
            return db.Companies.Count(e => e.CompanyID == id) > 0;
        }
    }
}