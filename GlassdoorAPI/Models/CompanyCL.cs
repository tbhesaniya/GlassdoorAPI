﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlassdoorAPI.Models
{
    public class CompanyCL
    {
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<int> StateID { get; set; }
        public Nullable<int> CountryID { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public Nullable<int> CompanyTypeID { get; set; }
        public Nullable<int> CompanySectorID { get; set; }
        public string CompanyStatus { get; set; }
        public string CompanyContactName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<int> IdConfirmUser { get; set; }
        public string SecurityCode { get; set; }
        public Nullable<int> CompanyStatusID { get; set; }
        public string Email { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string CompanyType { get; set; }
        public string CompanySector { get; set; }
        public string Description { get; set; }

        public List<Company> CompaniesList { get; set; }
        public Company SelectedCompany { get; set; }
        public string DisplayMode { get; set; }
    }
}