﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlassdoorAPI.Models
{
    public class companyInfo
    {
        public int CompanyID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public string Type { get; set; }
        public string Sector { get; set; }
        public string Status { get; set; }

        public string ContactName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}