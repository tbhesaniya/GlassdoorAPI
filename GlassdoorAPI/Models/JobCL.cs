﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlassdoorAPI.Models
{
    public class JobCL
    {
        public int JobID { get; set; }
        public string JobName { get; set; }
        public string JobDescription { get; set; }
        public int CompanyID { get; set; }
        public Nullable<System.DateTime> DateCreated2 { get; set; }
        public string DateCreated { get; set; }
        public string Status { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Company { get; set; }
        public Nullable<int> DaysSponsered { get; set; }
        public Nullable<int> StateID { get; set; }
        public Nullable<int> CountryID { get; set; }
        public int Clicks { get; set; }



        public List<Job> JobList { get; set; }
        public Company SelectedJob { get; set; }
        public string DisplayMode { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }



    }
}