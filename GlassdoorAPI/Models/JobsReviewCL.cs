﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlassdoorAPI.Models
{
    public class JobsReviewCL
    {
        public int ReviewID { get; set; }
        public Nullable<int> JobID { get; set; }
        public Nullable<int> EmployeeID { get; set; }
        public Nullable<int> Raiting { get; set; }
        public Nullable<System.DateTime> DateT { get; set; }
        public string Comments { get; set; }
        public string Name { get; set; }
        public string Profession { get; set; }
        public string JobName { get; set; }
        public string CompanyName { get; set; }
}
}