//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlassdoorAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Company
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Company()
        {
            this.Jobs = new HashSet<Job>();
            this.JobsReviews = new HashSet<JobsReview>();
        }
    
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<int> StateID { get; set; }
        public Nullable<int> CountryID { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public Nullable<int> CompanyTypeID { get; set; }
        public Nullable<int> CompanySectorID { get; set; }
        public string CompanyStatus { get; set; }
        public string CompanyContactName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<int> IdConfirmUser { get; set; }
        public string SecurityCode { get; set; }
        public Nullable<int> CompanyStatusID { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public byte[] Logo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Job> Jobs { get; set; }
        public virtual CompanySector CompanySector { get; set; }
        public virtual CompanyStatu CompanyStatu { get; set; }
        public virtual CompanyType CompanyType { get; set; }
        public virtual Country Country { get; set; }
        public virtual State State { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobsReview> JobsReviews { get; set; }
    }
}
