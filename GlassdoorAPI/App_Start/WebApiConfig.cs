﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GlassdoorAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            string origin = "http://localhost:59915/";
            EnableCorsAttribute cors = new EnableCorsAttribute(origin, "*", "GET,POST, DELETE, PUT");
            config.EnableCors(cors);


            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
               name: "secondApi",
               routeTemplate: "second/{controller}/{action}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            config.Routes.MapHttpRoute(
                name: "DashboardApi",
                routeTemplate: "dashboard/{controller}/{action}",
                defaults: new { controller = "Cpydashboard" }
            );


        }
    }
}
